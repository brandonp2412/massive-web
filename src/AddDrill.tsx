import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import * as drillService from "./services/drill.service";
import DrillModel from "./models/drill.model";
import DrillForm from "./DrillForm";

export default function AddDrill() {
  const history = useHistory();
  const [firstDrill, setFirstDrill] = useState<DrillModel>();

  useEffect(() => {
    async function updateDrill() {
      const drills = await drillService.get({
        limit: 1,
        offset: 0,
        sortBy: "date",
        sortDirection: "desc",
      });
      let drill: DrillModel = drills[0];
      if (!drill) drill = { date: "", name: "", reps: 0, weight: 0 };
      delete drill.id;
      drill.date = new Date().toISOString();
      setFirstDrill(drill);
    }
    updateDrill();
  }, []);

  const save = async (drill: DrillModel) => {
    await drillService.add(drill);
    history.push("/table");
  };

  return firstDrill ? <DrillForm save={save} value={firstDrill} /> : null;
}
