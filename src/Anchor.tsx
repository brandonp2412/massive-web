import { AnchorHTMLAttributes } from "react";

export default function Anchor(props: AnchorHTMLAttributes<HTMLAnchorElement>) {
  return (
    <a {...props} style={{ color: "aqua", ...props.style }}>
      {props.children}
    </a>
  );
}
