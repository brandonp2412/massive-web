import React, { useState } from "react";
import DrillModel from "./models/drill.model";
import * as drillService from "./services/drill.service";
import Snackbar from "./Snackbar";
import Spinner from "./Spinner";

export default function Settings() {
  const [loading, setLoading] = useState(false);
  const [snackbar, setSnackbar] = useState("");
  const file = React.createRef<HTMLInputElement>();

  return (
    <div id="settings" style={{ display: "flex", flexDirection: "column" }}>
      {loading && <Spinner />}

      <div>
        <button
          className="primary"
          style={{ marginBottom: ".25rem" }}
          id="export"
          onClick={async () => {
            const drills = await drillService.getAll(),
              fileName = `drills-${
                new Date().toISOString().split("T")[0]
              }.json`,
              file = new File([JSON.stringify(drills)], fileName, {
                type: "text/json",
              }),
              a = document.createElement("a"),
              url = URL.createObjectURL(file);
            a.href = url;
            a.download = fileName;
            document.body.appendChild(a);
            a.click();
            setTimeout(() => {
              document.body.removeChild(a);
              window.URL.revokeObjectURL(url);
            });
          }}
        >
          Download
        </button>
      </div>

      <input
        type="file"
        style={{ display: "none" }}
        ref={file}
        onChange={async (event) => {
          if (!event.target?.files?.[0]) return;
          try {
            setLoading(true);
            const readFile = (file: File): Promise<string> => {
              return new Promise((resolve, _reject) => {
                const reader = new FileReader();
                reader.onload = () => {
                  resolve(reader.result?.toString() || "");
                };
                reader.readAsText(file);
              });
            };
            const text = await readFile(event.target.files[0]);
            const drills: DrillModel[] = JSON.parse(text);
            await drillService.clear();
            await Promise.all(
              drills.map((drill) => drillService.add(drill, false))
            );
            setSnackbar("Uploaded drills.");
            setTimeout(() => setSnackbar(""), 3000);
          } finally {
            setLoading(false);
          }
        }}
      />
      <div>
        <button
          onClick={() => {
            const ok = confirm(
              "Are you sure you want to upload new drills? This will overwrite all current data."
            );
            if (!ok) return;
            file?.current?.click();
          }}
          style={{ marginBottom: ".25rem" }}
        >
          Upload
        </button>
      </div>

      <div>
        <button
          className="primary"
          style={{ marginBottom: ".25rem" }}
          onClick={async () => {
            const ok = confirm(
              "Are you sure you want to delete all data? You might want to back up with Download first."
            );
            if (!ok) return;
            setLoading(true);
            try {
              localStorage.clear();
              await drillService.clear();
              setSnackbar("Cleared data.");
              setTimeout(() => setSnackbar(""), 3000);
            } finally {
              setLoading(false);
            }
          }}
        >
          Clear data
        </button>
      </div>

      {snackbar && <Snackbar message={snackbar} />}
    </div>
  );
}
