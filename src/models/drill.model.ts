export default interface DrillModel {
  id?: number;
  name: string;
  reps: number;
  weight: number;
  date: string;
}
