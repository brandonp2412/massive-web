import { Chart, registerables } from "chart.js";
import { createRef, useEffect, useState } from "react";
import "./Graph.css";
import DrillModel from "./models/drill.model";
import { formatDate, getWeek } from "./services/date.service";
import { get, getNames } from "./services/drill.service";

let chart: Chart;

Chart.register(...registerables);
Chart.defaults.color = "white";

export default function Graph() {
  const [names, setNames] = useState<string[]>([]);
  const [name, setName] = useState("");
  const [begin, setBegin] = useState("");
  const [end, setEnd] = useState("");
  const [duration, setDuration] = useState("Daily");
  const canvas = createRef<HTMLCanvasElement>();

  useEffect(() => {
    async function updateNames() {
      setNames(await getNames());
      const drills = await get({
        limit: 1,
        offset: 0,
        sortBy: "date",
        sortDirection: "desc",
      });
      if (!drills.length) return;
      setName(drills[0].name);
    }
    updateNames();
  }, []);

  const groupByDay = (drills: DrillModel[]): { [date: string]: DrillModel[] } =>
    drills
      .filter((drill) => {
        const drillDate = new Date(drill.date);
        const beginDate = new Date(begin);
        const endDate = new Date(end);
        if (begin && !end) return beginDate <= drillDate;
        if (end && !begin) return endDate >= drillDate;
        if (begin && end) return beginDate <= drillDate && endDate >= drillDate;
        return true;
      })
      .sort(
        (drillA, drillB) =>
          Number(new Date(drillA.date)) - Number(new Date(drillB.date))
      )
      .reduce<{ [date: string]: DrillModel[] }>((previous, current) => {
        let day: string;
        const date = new Date(current.date);
        if (duration === "Daily") day = formatDate(current.date);
        else if (duration === "Weekly")
          day = `${date.getFullYear()}-${date.getMonth() + 1}-${getWeek(date)}`;
        else day = `${date.getFullYear()}-${date.getMonth() + 1}`;
        previous[day] = previous[day] ? [current, ...previous[day]] : [current];
        return previous;
      }, {});

  const getReports = (dailies: { [date: string]: DrillModel[] }) => {
    const days = Object.keys(dailies);
    const volumes = days.map(
      (day) =>
        dailies[day].reduce(
          (previous, current) => previous + current.reps * current.weight,
          0
        ) / 100
    );
    const reps = days.map((day) => {
      const total = dailies[day].reduce(
        (previous, current) => previous + current.reps,
        0
      );
      return total / dailies[day].length;
    });
    const weights = days.map((day) => {
      const total = dailies[day].reduce(
        (previous, current) => previous + current.weight,
        0
      );
      return total / dailies[day].length;
    });
    return { days, volumes, reps, weights };
  };

  const makeChart = ({
    context,
    volumes,
    weights,
    reps,
    days,
  }: {
    context: CanvasRenderingContext2D;
    volumes: number[];
    weights: number[];
    reps: number[];
    days: string[];
  }) => {
    return new Chart(context, {
      type: "line",
      data: {
        labels: days,
        datasets: [
          {
            label: "Volume (x100)",
            borderColor: "white",
            data: volumes,
            yAxisID: "kgs",
          },
          {
            label: "Weight",
            borderColor: "pink",
            data: weights,
            yAxisID: "kgs",
          },
          {
            label: "Reps",
            borderColor: "aqua",
            data: reps,
            yAxisID: "reps",
          },
        ],
      },
      options: {
        plugins: { legend: { labels: { color: "white" } } },
      },
    });
  };

  useEffect(() => {
    const getData = async () => {
      if (!name) return [];
      const drills = await get({
        limit: 0,
        offset: 0,
        sortBy: "date",
        sortDirection: "desc",
        term: name,
      });
      if (!drills) return;
      const grouped = groupByDay(drills);
      const { days, volumes, reps, weights } = getReports(grouped);
      if (chart) chart.destroy();
      const context = canvas.current?.getContext("2d");
      if (!context) return;
      chart = makeChart({ context, volumes, weights, reps, days });
      chart.update();
    };
    getData();
  }, [name, begin, end, duration]);

  return (
    <div id="graph">
      <canvas ref={canvas} width="350" height="350"></canvas>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "100px 250px",
          justifyContent: "center",
          alignItems: "center",
          gap: "5px",
        }}
      >
        <p>Drill</p>
        <select
          value={name}
          onChange={async (event) => {
            setName(event.target.value);
          }}
          style={{ marginBottom: 0 }}
        >
          {names.map((name) => (
            <option key={name} value={name}>
              {name}
            </option>
          ))}
        </select>
        <p>Duration</p>
        <select
          value={duration}
          onChange={async (event) => {
            setDuration(event.target.value);
          }}
        >
          <option>Daily</option>
          <option>Weekly</option>
          <option>Monthly</option>
        </select>
        <p>Begin</p>

        <input
          type="date"
          value={begin}
          onChange={(e) => setBegin(e.target.value)}
        />

        <p>End</p>
        <input
          type="date"
          value={end}
          onChange={(e) => setEnd(e.target.value)}
        />
      </div>
    </div>
  );
}
