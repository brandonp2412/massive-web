/**
 * @returns yyyy-mm-dd
 */
export function formatDate(date: string) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}

/**
 * Returns the week number for this date.  0 is the day of week the week
 * "starts" on for your locale - it can be from 0 to 6. If 0 is 1 (Monday),
 * the week returned is the ISO 8601 week number.
 * @param int 0
 * @return int
 */
export const getWeek = function (date: Date) {
  /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */

  const newYear = new Date(date.getFullYear(), 0, 1);
  let day = newYear.getDay() - 0; //the day of week the year begins on
  day = day >= 0 ? day : day + 7;
  const dayNum =
    Math.floor(
      (date.getTime() -
        newYear.getTime() -
        (date.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) /
        86400000
    ) + 1;
  let weekNumber;
  //if the year starts before the middle of a week
  if (day < 4) {
    weekNumber = Math.floor((dayNum + day - 1) / 7) + 1;
    if (weekNumber > 52) {
      const nYear = new Date(date.getFullYear() + 1, 0, 1);
      let nDay = nYear.getDay() - 0;
      nDay = nDay >= 0 ? nDay : nDay + 7;
      /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
      weekNumber = nDay < 4 ? 1 : 53;
    }
  } else {
    weekNumber = Math.floor((dayNum + day - 1) / 7);
  }
  return weekNumber;
};
