import DrillModel from "../models/drill.model";
import Dexie from "dexie";

const dexieDb = new Dexie("massive");
dexieDb.version(0.3).stores({ drills: "++id,name,date" });
const drillsTable = dexieDb.table("drills");

export const CHANGE_EVENT = "drill-change";

export async function add(drill: DrillModel, dispatch: boolean = true) {
  delete drill.id;
  await drillsTable.add(drill);
  if (dispatch) window.dispatchEvent(new Event(CHANGE_EVENT));
}

export async function edit(drill: DrillModel) {
  await drillsTable.put(drill);
  window.dispatchEvent(new Event(CHANGE_EVENT));
}

export async function remove(id: number) {
  await drillsTable.delete(id);
  window.dispatchEvent(new Event(CHANGE_EVENT));
}

export async function length(term?: string, today?: boolean) {
  let drills = term
    ? await drillsTable.where("name").startsWithIgnoreCase(term).toArray()
    : await drillsTable.toArray();
  if (today)
    drills = drills.filter(
      (drill: DrillModel) =>
        new Date(drill.date).toDateString() === new Date().toDateString()
    );
  return drills.length;
}

export const get = async ({
  limit,
  offset,
  sortBy,
  sortDirection,
  term,
}: {
  limit: number;
  offset: number;
  sortBy: string;
  sortDirection: "asc" | "desc";
  term?: string;
}) => {
  let collection = term
    ? drillsTable.filter((drill: DrillModel) =>
        drill.name.toLowerCase().includes(term.toLowerCase())
      )
    : drillsTable.toCollection();
  if (limit) collection = collection.offset(offset).limit(limit);
  if (sortDirection === "desc") collection = collection.reverse();
  return collection.sortBy(sortBy);
};

export async function getNames(): Promise<string[]> {
  const drills: DrillModel[] = await drillsTable.toCollection().sortBy("name");
  return drills
    .map((drill) => drill.name)
    .filter((value, index, array) => array.indexOf(value) === index);
}

export async function getAll(): Promise<DrillModel[]> {
  return drillsTable.toArray();
}

export async function clear() {
  return drillsTable.clear();
}

export async function addMany(drills: DrillModel[]) {
  return drillsTable.bulkAdd(drills);
}

export async function find(id: number) {
  return drillsTable.get(id);
}
