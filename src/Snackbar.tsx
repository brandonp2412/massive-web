export default function Snackbar({
  message,
  onUndo,
}: {
  message: string;
  onUndo?: () => void;
}) {
  return (
    <div
      style={{
        minWidth: 300,
        backgroundColor: "#333",
        color: "#fff",
        position: "fixed",
        bottom: 30,
        textAlign: "center",
        marginLeft: -150,
        left: "50%",
        right: "50%",
        borderRadius: 5,
        padding: 10,
        zIndex: 1,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div>{message}</div>
      {onUndo && (
        <button style={{ marginLeft: "auto" }} onClick={onUndo}>
          Undo
        </button>
      )}
    </div>
  );
}
