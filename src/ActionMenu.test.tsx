import { fireEvent, render, screen } from "@testing-library/react";
import { ActionMenu } from "./ActionMenu";

jest.mock("idb/with-async-ittr.js");

const drill = {
  date: "2021-03-23T00:09:20.400Z",
  name: "Squats",
  reps: 10,
  weight: 20,
  unit: "kg",
  id: 1,
};

test("renders Action Menu button by default", () => {
  render(<ActionMenu drill={drill} onRemoved={() => {}} />);
  const menu = screen.getByText(/⋯/g);
  expect(menu).toBeInTheDocument();
});
test("Renders Action Menu contents when ⋯ is clicked", () => {
  render(<ActionMenu drill={drill} onRemoved={() => {}} />);
  const button = screen.getByRole("button");

  fireEvent.click(button);
  expect(screen.getByText(/Edit/g)).toBeInTheDocument();
  expect(screen.getByText(/Delete/g)).toBeInTheDocument();

  fireEvent.click(button);
  expect(screen.queryByText(/Edit/g)).toBe(null);
  expect(screen.queryByText(/Delete/g)).toBe(null);
});
