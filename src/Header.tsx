import React, { ReactNode } from "react";
import { Link, useLocation } from "react-router-dom";
import { Path } from "./Routes";
import "./Header.css";

export default function Header() {
  const location = useLocation();

  const LinkTo = ({ to, children }: { to: string; children: ReactNode }) => (
    <Link
      className={`link-style${location.pathname === to ? " active" : ""}`}
      to={to}
    >
      {children}
    </Link>
  );

  return (
    <div className="tabs">
      <LinkTo to={Path.History}>History</LinkTo>
      <LinkTo to={Path.Graph}>Graphs</LinkTo>
      <LinkTo to={Path.Settings}>Settings</LinkTo>
    </div>
  );
}
