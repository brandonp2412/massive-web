import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import DrillForm from "./DrillForm";
import DrillModel from "./models/drill.model";
import * as drillService from "./services/drill.service";

export default function EditDrill() {
  const history = useHistory();
  const params = useParams<{ id: string }>();
  const [drill, setDrill] = useState<DrillModel>();

  useEffect(() => {
    drillService.find(Number(params.id)).then(setDrill);
  }, []);

  if (!drill) return <></>;

  return (
    <DrillForm
      save={async (editedDrill: DrillModel) => {
        await drillService.edit(editedDrill);
        history.push("/");
      }}
      value={drill}
    />
  );
}
