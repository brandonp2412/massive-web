import { BrowserRouter } from "react-router-dom";
import "./App.css";
import Header from "./Header";
import Routes from "./Routes";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Routes />
      </div>
    </BrowserRouter>
  );
}

export default App;
