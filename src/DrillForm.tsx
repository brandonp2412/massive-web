import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import DrillModel from "./models/drill.model";
import { formatDate } from "./services/date.service";
import { get, getNames } from "./services/drill.service";

export const labelStyle: React.CSSProperties = {
  fontSize: 22,
};

export const inputStyle: React.CSSProperties = {
  fontSize: 22,
  marginLeft: 5,
  marginRight: 5,
  display: "inline-block",
  width: "100%",
};

export const gridStyle: React.CSSProperties = {
  display: "grid",
  gridTemplateColumns: "1fr 1fr",
  justifyContent: "center",
};

export const formStyle: React.CSSProperties = {
  height: "75vh",
  marginTop: 5,
  display: "flex",
  flexDirection: "column",
  marginLeft: "auto",
  marginRight: "auto",
  paddingLeft: "3rem",
  paddingRight: "3rem",
};

export default function DrillForm({
  save,
  value,
}: {
  save: (drill: DrillModel) => void;
  value: DrillModel;
}) {
  const history = useHistory();
  const [names, setNames] = useState<string[]>([]);
  const [name, setName] = useState("");
  const [reps, setReps] = useState("");
  const [weight, setWeight] = useState("");
  const [date, setDate] = useState(value.date || new Date().toISOString());

  useEffect(() => {
    setName(value.name);
    setReps(value.reps.toString());
    setWeight(value.weight.toString());
    async function updateNames() {
      const newNames = await getNames();
      setNames(newNames);
    }
    updateNames();
  }, [value]);

  return (
    <>
      <form style={formStyle}>
        <label htmlFor="name" style={labelStyle}>
          Name
        </label>
        <input
          id="name"
          style={inputStyle}
          value={name}
          onKeyDown={(event) => {
            if (event.key !== "Backspace") return;
            setName("");
          }}
          onChange={async (event) => {
            setName(event.target.value);
            const drills = await get({
              limit: 1,
              offset: 0,
              sortBy: "date",
              sortDirection: "desc",
              term: event.target.value,
            });
            if (!drills.length) return;
            setReps(drills[0].reps.toString());
            setWeight(drills[0].weight.toString());
          }}
          list="names"
          autoFocus
        />
        <datalist id="names">
          {names.map((nameOption) => (
            <option key={nameOption} value={nameOption} />
          ))}
        </datalist>
        <label htmlFor="reps" style={labelStyle}>
          Reps
        </label>
        <input
          id="reps"
          style={inputStyle}
          value={reps}
          onChange={(e) => setReps(e.target.value)}
          type="number"
          autoFocus
        />
        <label htmlFor="weight" style={labelStyle}>
          Weight
        </label>
        <input
          id="weight"
          style={inputStyle}
          value={weight}
          onChange={(e) => setWeight(e.target.value)}
          type="number"
        />
        <label htmlFor="date" style={labelStyle}>
          Date
        </label>
        <input
          id="date"
          style={inputStyle}
          value={formatDate(date)}
          onChange={(e) => setDate(new Date(e.target.value).toISOString())}
          type="date"
        />

        <div>
          <button
            type="button"
            onClick={(event) => {
              event.preventDefault();
              const editedDrill = {
                name,
                reps: Number(reps),
                weight: Number(weight),
                date,
              };
              if (value.id) return save({ ...editedDrill, id: value.id });
              return save(editedDrill);
            }}
            className="primary"
            style={{ marginRight: "1rem", marginTop: "1rem" }}
          >
            Save
          </button>
          <button type="button" onClick={() => history.push("/")}>
            Cancel
          </button>
        </div>
      </form>
    </>
  );
}
