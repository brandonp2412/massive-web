import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { LIMIT } from "./constants";
import DrillModel from "./models/drill.model";
import * as drillService from "./services/drill.service";
import Table from "./Table";
import { Path } from "./Routes";
import Snackbar from "./Snackbar";

export default function History() {
  const [drills, setDrills] = useState<DrillModel[]>();
  const [offset, setOffset] = useState(0);
  const [pages, setPages] = useState(0);
  const [snackbar, setSnackbar] = useState("");
  const [removed, setRemoved] = useState<DrillModel | null>(null);
  const [sortBy, setSortBy] = useState("date");
  const [sortDirection, setSortDirection] = useState<"asc" | "desc">("desc");
  const [today, setToday] = useState(false);
  const history = useHistory();
  const [term, setTerm] = useState("");

  useEffect(() => {
    window.addEventListener(drillService.CHANGE_EVENT, () => {
      refresh();
    });
    const savedToday = localStorage.getItem("today");
    if (!savedToday) return;
    setToday(savedToday === "true");
  }, []);

  useEffect(() => {
    localStorage.setItem("today", today.toString());
  }, [today]);

  useEffect(() => {
    refresh();
  }, [offset, sortBy, sortDirection, term, today]);

  const refresh = async () => {
    let newDrills: DrillModel[] = await drillService.get({
      limit: LIMIT,
      offset,
      sortBy,
      sortDirection,
      term,
    });
    if (today)
      newDrills = newDrills.filter(
        (drill) =>
          new Date(drill.date).toDateString() === new Date().toDateString()
      );
    setDrills(newDrills);
    setPages(Math.ceil((await drillService.length(term, today)) / LIMIT));
  };

  const undo = async () => {
    if (!removed) return;
    await drillService.add(removed);
    setRemoved(null);
    setSnackbar("");
  };

  return (
    <div>
      <div style={{ margin: 10, display: "flex", alignItems: "center" }}>
        <input
          value={term}
          onChange={(e) => setTerm(e.target.value)}
          placeholder="Search"
        />
      </div>
      <Table
        sortBy={sortBy}
        sortDirection={sortDirection}
        setSortBy={setSortBy}
        setSortDirection={setSortDirection}
        onRemoved={(drill) => {
          setRemoved(drill);
          setSnackbar(`Removed ${drill.name}`);
          setTimeout(() => {
            setSnackbar("");
          }, 4000);
        }}
        drills={drills}
        today={today}
      />
      <button
        style={{ marginRight: ".5rem" }}
        onClick={() => setOffset(offset - LIMIT)}
        disabled={offset <= 0}
      >
        Prev
      </button>
      <button
        style={{ marginRight: ".5rem" }}
        className="primary"
        onClick={() => history.push(Path.Add)}
      >
        Add drill
      </button>
      <button
        onClick={() => setOffset(offset + LIMIT)}
        disabled={Number(drills?.length) < LIMIT}
      >
        Next
      </button>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyItems: "center",
          marginTop: "1rem",
        }}
      >
        <label style={{ marginLeft: 20 }}>
          Today
          <input
            type="checkbox"
            onChange={(event) => setToday(event.target.checked)}
            checked={today}
          />
        </label>
        <span style={{ marginLeft: "auto", marginRight: 20 }}>
          Page: {offset / LIMIT + 1} of {pages}.
        </span>
      </div>
      {snackbar && <Snackbar onUndo={undo} message={snackbar} />}
    </div>
  );
}
