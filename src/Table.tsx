import DrillModel from "./models/drill.model";
import "./Table.css";
import { ActionMenu } from "./ActionMenu";

export default function Table({
  drills,
  onRemoved,
  sortBy,
  sortDirection,
  setSortBy,
  setSortDirection,
  today,
}: {
  drills?: DrillModel[];
  onRemoved: (drill: DrillModel) => void;
  sortBy: string;
  sortDirection: "asc" | "desc";
  setSortBy: (by: string) => void;
  setSortDirection: (direction: "asc" | "desc") => void;
  today?: boolean;
}) {
  const TableHeader = ({ children }: { children: string }) => (
    <th
      style={{ cursor: "pointer" }}
      onClick={() => {
        setSortDirection(sortDirection === "asc" ? "desc" : "asc");
        setSortBy(children.toLowerCase());
      }}
    >
      {children}{" "}
      {sortBy === children.toLowerCase()
        ? sortDirection === "asc"
          ? "▲"
          : "▼"
        : null}
    </th>
  );

  if (!drills) return <div className="table-container"></div>;

  return (
    <div className="table-container">
      <table>
        <thead>
          <tr>
            <TableHeader>Name</TableHeader>
            <TableHeader>Reps</TableHeader>
            <TableHeader>Weight</TableHeader>
            <TableHeader>Date</TableHeader>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {drills.map((drill) => (
            <tr key={drill.id}>
              <td>{drill.name}</td>
              <td>{drill.reps}</td>
              <td>{drill.weight}kg</td>
              <td>
                {today
                  ? new Date(drill.date).toLocaleTimeString()
                  : new Date(drill.date).toLocaleString()}
              </td>
              <td>
                <ActionMenu drill={drill} onRemoved={onRemoved} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
