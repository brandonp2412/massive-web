import { useHistory } from "react-router";
import DrillModel from "./models/drill.model";
import * as drillService from "./services/drill.service";
import { Path } from "./Routes";
import "./ActionMenu.css";
import { useState } from "react";

export const ActionMenu = ({
  drill,
  onRemoved,
}: {
  drill: DrillModel;
  onRemoved: (drill: DrillModel) => void;
}) => {
  const history = useHistory();
  const [showDropdown, setShowDropdown] = useState(false);

  return (
    <div className="dropdown">
      <button
        onClick={() => {
          setShowDropdown(!showDropdown);
        }}
        className="dropdownBtn icon"
      >
        ...
      </button>
      {showDropdown && (
        <div className="dropdown-content">
          <button
            onClick={() =>
              history.push(Path.Edit.replace(":id", drill.id?.toString() || ""))
            }
          >
            Edit
          </button>
          <button
            onClick={async () => {
              await drillService.remove(Number(drill.id));
              onRemoved(drill);
            }}
          >
            Delete
          </button>
        </div>
      )}
    </div>
  );
};
