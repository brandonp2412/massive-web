import { Redirect, Route, Switch } from "react-router";
import AddDrill from "./AddDrill";
import EditDrill from "./EditDrill";
import Graph from "./Graph";
import History from "./History";
import Settings from "./Settings";

export enum Path {
  Graph = "/graph",
  Add = "/add/drill",
  Edit = "/edit/drill/:id",
  History = "/history",
  Settings = "/settings",
}

export default function Routes() {
  return (
    <Switch>
      <Route path={Path.Graph}>
        <Graph />
      </Route>
      <Route path={Path.Add}>
        <AddDrill />
      </Route>
      <Route path={Path.Edit}>
        <EditDrill />
      </Route>
      <Route path={Path.History}>
        <History />
      </Route>
      <Route path={Path.Settings}>
        <Settings />
      </Route>
      <Route path="/">
        <Redirect to={Path.History} />
      </Route>
    </Switch>
  );
}
