# Screenshots

<img src="screenshots/history.png" width="300" />
<img src="screenshots/graphs.png" width="300" />

# Installing

This project requires [Node.js](https://nodejs.org/en/).

```shell
npm install
```

# Running

```
npm start
```

# Building for production

```
npm run build
```

Now your website is in the `build/` directory.

# Installing git hooks

```shell
git config core.hooksPath ./hooks
```

# Relevant Documentation

- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/javascript)
- [TypeScript](https://www.typescriptlang.org/docs/handbook/intro.html)
- [React](https://reactjs.org/docs/getting-started.html)
- [React Router](https://reactrouter.com/web/guides/quick-start)
- [Chart.js](https://www.chartjs.org/docs/latest/)
- [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API)
- [Dexie](https://dexie.org/docs/)
